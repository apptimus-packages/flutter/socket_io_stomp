import 'package:socket_io_stomp/stomp_frame.dart';

abstract class Parser {
  bool escapeHeaders;

  void parseData(dynamic data);

  dynamic serializeFrame(StompFrame frame);
}
