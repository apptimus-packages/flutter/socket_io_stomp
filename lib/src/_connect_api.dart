import 'dart:async';

import 'package:socket_io_stomp/stomp_config.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

Future<WebSocketChannel> connect(StompConfig config) {
  throw UnsupportedError('No implementation of the connect api provided');
}
